<?php
/**
 * Created by PhpStorm.
 * User: ANDROMEDA
 * Date: 2/19/2018
 * Time: 7:36 PM
 */
namespace App\Core\Contracts\Databases;

interface ContentTableInterfaces
{
    /**
     * Get all of the images for the posts and users
     *
     * @return Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function media();
}