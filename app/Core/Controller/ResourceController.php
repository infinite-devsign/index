<?php
/**
 * Created by PhpStorm.
 * User: aelit
 * Date: 2/15/2018
 * Time: 12:43 PM
 */
namespace App\Core\Controller;

use Illuminate\Http\Request;
use Illuminate\View\View;

trait ResourceController
{
    public function setData($key,$value)
    {
        View::share($key, $value);
        return $this;
    }


}