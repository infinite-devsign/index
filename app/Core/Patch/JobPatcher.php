<?php
/**
 * Created by PhpStorm.
 * User: ANDROMEDA
 * Date: 2/15/2018
 * Time: 2:14 PM
 */
namespace App\Core\Patch;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;

abstract class JobPatcher
{
    use AuthorizesRequests, ValidatesRequests;

    const SUCCESS = 'success';
    const FAILED = 'failed';

    protected $request;

    protected $validateRules = [];

    protected $status = 'idle';

    /**
     * JobPatcher constructor.
     * @param Request $request
     */
    public function __construct($request)
    {
        $this->request = $request;
    }

    public function handle()
    {
        $this->validate($this->request, $this->validateRules);
        $process = $this->run();
        $this->status = ($process) ? self::SUCCESS : self::FAILED;
    }


    /**
     * @return mixed
     */
    abstract public function run();


    /**
     * @param string $callback
     * @return bool
     * check while job is success
     * and execute a callback
     */
    public function success($callback = '')
    {
        if ($this->status === self::SUCCESS) {
            if (!$callback)
                return true;

            return $callback($this->request);
        }

        return false;
    }

    /**
     * @param string $callback
     * @return bool
     * Check while job is failed
     * and execute a callback
     */
    public function failed($callback = '')
    {
        if ($this->status === self::FAILED) {
            if (!$callback)
                return true;

            return $callback($this->request);
        }

        return false;
    }
}