<?php

namespace App\Http\Controllers\Auth;

use App\Jobs\Accounts\AttemptUser;
use App\Models\Accounts\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    /**
     * AdminController constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->middleware('guest')->except('logout');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showLoginForm()
    {
        return view('auth.login');
    }

    public function login()
    {
        $role = User::ADMIN;
        try {
            $this->dispatch(new AttemptUser($this->request, $role));
        } catch (\Exception $exception) {
            return redirect()->back()->withErrors('username atau email, dan password tidak cocok');
        }

        return redirect()->route()->with('Selamat datang');
    }

    public function logout()
    {
        Auth::logout();
        return redirect();
    }
}
