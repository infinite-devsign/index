<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Jobs\Accounts\AttemptUser;
use App\Jobs\Accounts\AttemptUserWithSocial;
use App\Jobs\Accounts\CreateSocialUser;
use App\Jobs\Contents\CreateMediaFromSocial;
use App\Models\Accounts\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class LoginController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm()
    {
        return view();
    }

    public function login()
    {
        $role = User::MEMBER;
        try {
            $this->dispatch(new AttemptUser($this->request, $role));
        } catch (\Exception $exception) {
            return redirect()->back()->withErrors('username atau email, dan password tidak cocok');
        }

        return redirect()->route()->with('Selamat datang');
    }

    public function social($service)
    {
        return Socialite::driver($service)->redirect();
    }

    /**
     * @param $service
     */
    public function callback($service)
    {
        $user = Socialite::with($service)->user();

        /**
         * Check if doesnt exist
         **/
        if (User::where('email', $user->email)->doesntExist()) {
            try {
                $this->dispatch(new CreateSocialUser($user));
            } catch (\Exception $exception) {
                return redirect()->back();
            }
        }

        try {
            $this->dispatch(new AttemptUserWithSocial($user));
        } catch (\Exception $exception) {
            return redirect()->back();
        }

        return redirect()->route('home')->withDetails($user)->withService($service);
    }

    public function logout()
    {
        Auth::logout();
        return redirect();
    }
}
