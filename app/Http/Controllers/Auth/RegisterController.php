<?php

namespace App\Http\Controllers\Auth;

use App\Jobs\Accounts\CreateUser;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->middleware('guest');
    }

    public function register()
    {
        try {
            $this->dispatch(new CreateUser($this->request));
        } catch (\Exception $exception) {
            return redirect()->back()->withErrors('Some errors');
        }

        return redirect()->route('home')->with('Akun telah terdaftar, silahkan login ');
    }
}
