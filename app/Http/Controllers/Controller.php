<?php

namespace App\Http\Controllers;

use App\Core\Controller\ResourceController;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests, ResourceController;

    public $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }
}
