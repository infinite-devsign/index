<?php
/**
 * Created by PhpStorm.
 * User: ANDROMEDA
 * Date: 2/15/2018
 * Time: 9:25 PM
 */

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Jobs\Accounts\CreateSocialUser;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function __construct(Request $request)
    {
        parent::__construct($request);

        $this->middleware('auth.check:member');
    }

    public function index()
    {

    }

    public function create()
    {
        return view();
    }

    public function store()
    {
        try {
            $this->dispatch(new CreateSocialUser($this->request));
        } catch (\Exception $exception) {
            return redirect()->back()->withErrors('Error data not valid');
        }

        return redirect()->route();
    }

    public function edit()
    {
        return view();
    }

    public function update()
    {
        try {
            $this->dispatch();
        } catch (\Exception $exception) {
            return redirect()->back()->withErrors('Error data not valid');
        }

        return redirect()->route();
    }

    public function delete()
    {
        try {
            $this->dispatch();
        } catch (\Exception $exception) {
            return redirect()->back()->withErrors('Some error');
        }
    }
}
