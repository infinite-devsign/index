<?php

namespace App\Http\Controllers\Users;

use App\Models\Accounts\UserMetaData;
use App\Jobs\Accounts\CreateUserMetaData;
use Illuminate\Http\Request;

class UserMetaDataController extends Controller
{
    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->middleware('guest');
    }

    public function show(UserMetaData $data)
    {
        return view(compact($data));
    }

    public function create()
    {

    }

    public function store()
    {
        try {
            $this->dispatch(new CreateUserMetaData($this->request));
        } catch (\Exception $exception) {
            return redirect()->back()->withErrors('Some errors');
        }

        return redirect()->route();
    }

    public function edit()
    {
        return view();
    }

    public function update()
    {
        try {
            $this->dispatch(new UpdateUserMetaData($this->request));
        } catch (\Exception $exception) {
            return redirect()->back()->withErrors('Some errors');
        }

        return redirect()->route();
    }
}
