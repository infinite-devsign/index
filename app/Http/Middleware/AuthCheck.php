<?php

namespace App\Http\Middleware;

use App\Models\Accounts\User;
use Closure;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Support\Facades\Auth;

class AuthCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        if(!Auth::check())
            $this->doIfCheckFailed();

        $this->protectionRole($role);

        return $next($request);
    }

    protected function protectionRole($role)
    {
        switch ($role) {
            case User::ADMIN :
                if (Auth::user()->role != User::ADMIN)
                    $this->doIfCheckFailed();
                break;

            case User::MEMBER :
                if (Auth::user()->role != User::MEMBER)
                    $this->doIfCheckFailed();
                break;

            case User::GUEST :
                if (Auth::user()->role != User::GUEST)
                    $this->doIfCheckFailed();
                break;

            default :
                $this->doIfCheckFailed();
                break;
        }
    }

    protected function doIfCheckFailed()
    {
        throw new AuthenticationException('unauthenticated');
    }
}
