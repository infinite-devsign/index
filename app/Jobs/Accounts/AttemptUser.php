<?php

namespace App\Jobs\Accounts;

use App\Core\Patch\JobPatcher;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AttemptUser extends JobPatcher
{
    protected $validateRules = [
        'email' => 'required',
        'password' => 'required'
    ];

    protected $request;

    protected $role;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        parent::__construct($request);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function run()
    {
        return Auth::attempt(
            $this->credentials($this->request), $this->request->filled('remember')
        );
    }

    protected function credentials($request)
    {
        $field = filter_var($request->get($this->username()), FILTER_VALIDATE_EMAIL)
            ? $this->username() : 'username';

        return [
            $field => $request->get($this->username()),
            'password' => $request->password,
            'role' => $this->role ?: $this->role = 'guest'
        ];
    }

    protected function username()
    {
        return 'email';
    }
}
