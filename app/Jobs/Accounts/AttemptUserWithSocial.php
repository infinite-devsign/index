<?php
/**
 * Created by PhpStorm.
 * User: ANDROMEDA
 * Date: 2/20/2018
 * Time: 2:25 PM
 */

namespace App\Jobs\Accounts;


use App\Models\Accounts\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AttemptUserWithSocial
{
    protected $user;

    public function __construct($user)
    {
        $this->user = $user;
    }

    public function handle()
    {
        return Auth::login($this->credentials($this->user), true);
    }

    protected function credentials($user)
    {
        $login = User::where('email', $user->email)->first();
        return $login;
    }
}