<?php
/**
 * Created by PhpStorm.
 * User: ANDROMEDA
 * Date: 2/20/2018
 * Time: 12:37 PM
 */

namespace App\Jobs\Accounts;

use App\Core\Patch\JobPatcher;
use App\Jobs\Contents\CreateMediaFromSocial;
use App\Models\Accounts\User;
use Illuminate\Http\Request;

class CreateSocialUser
{
    protected $user;

    protected $model;

    public function __construct($user)
    {
        $this->user = $user;
        $this->model = new User();
    }

    public function handle()
    {
        $this->model->fill([
            'name' => $this->user->name,
            'username' => '',
            'email' => $this->user->email,
            'password' => '',
            'role' => 'guest',
            'remember_token' => str_random(10)
        ]);
        $this->model->save();

        dispatch(new CreateMediaFromSocial($this->user, $this->model));

        return $this->user;
    }
}