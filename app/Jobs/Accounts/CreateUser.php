<?php
/**
 * Created by PhpStorm.
 * User: ANDROMEDA
 * Date: 2/19/2018
 * Time: 9:44 AM
 */

namespace App\Jobs\Accounts;

use App\Core\Patch\JobPatcher;
use App\Events\Accounts\Users\WasCreated;
use App\Models\Accounts\User;
use Illuminate\Events\Dispatcher;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CreateUser extends JobPatcher
{
    protected $validateRules = [
        'name' => 'required',
        'username' => 'required|unique:users',
        'email' => 'required|unique:users|email',
        'password' => 'required|string|min:6|confirmed',
    ];

    public function __construct(Request $request)
    {
        parent::__construct($request);

    }

    /**
     * @param Dispatcher $event
     * @return $this|mixed
     */
    public function run()
    {
        $user = new User();
        $user->fill([
            'name' => $this->request->input('name'),
            'username' => $this->request->input('username'),
            'email' => $this->request->input('email'),
            'password' => $this->request->input('password'),
            'remember_token' => str_random(10),
        ]);
        $user->save();

        if ($user instanceof User)
            event(new WasCreated($user));

        $this->guard()->login($user);

        return $user;
    }

    protected function guard()
    {
        return Auth::guard();
    }
}