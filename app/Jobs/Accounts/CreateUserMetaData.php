<?php

namespace App\Jobs\Accounts;

use App\Core\Patch\JobPatcher;
use App\Models\Accounts\User;
use Illuminate\Http\Request;

class CreateUserMetaData extends JobPatcher
{
    protected $user;

    protected $validateRules = [
        'address' => 'string',
        'city' => 'string',
        'postal_code' => 'digits:5|integer',
        'birthday' => 'string',
        'gender' => 'string',
    ];

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->user = auth()->user();
    }

    public function run()
    {
        return $this->user
                    ->userMetaData()
                    ->create($this->request->all());
    }
}
