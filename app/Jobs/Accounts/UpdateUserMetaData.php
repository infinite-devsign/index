<?php

namespace App\Jobs\Accounts;

use App\Core\Patch\JobPatcher;
use App\Models\Accounts\User;
use Illuminate\Http\Request;

class UpdateUserMetaData extends JobPatcher
{
    protected $validateRules = [
        'address' => 'string',
        'city' => 'string',
        'postal_code' => 'digits:5|integer',
        'birthday' => 'string',
        'gender' => 'string',
    ];

    public function __construct(Request $request)
    {
        parent::__construct($request);
    }

    public function run()
    {
        $user = auth()->user();
        $user->userMetaData()->update($this->request->all());

        return $user;
    }
}
