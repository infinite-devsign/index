<?php
/**
 * Created by PhpStorm.
 * User: ANDROMEDA
 * Date: 2/19/2018
 * Time: 7:31 PM
 */

namespace App\Jobs\Contents;

use App\Core\Contracts\Databases\ContentTableInterfaces;
use App\Core\Patch\JobPatcher;
use App\Models\Contents\Media;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CreateMedia extends JobPatcher
{
    protected $validateRules = [
        'images.*' => 'image|mimes:jpeg,png,jpg,gif,svg'
    ];

    protected $model;
    /**
     * CreateMedia constructor.
     * @param Request $request
     */
    public function __construct(Request $request, ContentTableInterfaces $model)
    {
        parent::__construct($request);
        $this->model = $model;
    }

    public function run()
    {
        $this->model->media()->saveMany($this->uploadFiles());

        return $this->model;
    }

    protected function uploadFiles()
    {
        $uploaded = [];

        foreach ($this->request->images as $image) {
            $path = 'uploads/images';
            $realImage = hash('crc32b', uniqid()).'.'.$image->getClientOriginalExtension();

            $image->storeAs($path, $realImage);
            array_push($uploaded, new Media([
                'slug' => $this->getSlugByNameAndExt($image),
                'src' => $path . '/' . $realImage
            ]));
        }

        return $uploaded;
    }

    protected function getSlugByNameAndExt($image)
    {
        $str = hash('crc32b', $image->getClientOriginalname() . uniqid()) . '-' . Carbon::now() .
            '-' . $image->getClientOriginalExtension();
        return str_slug($str);
    }
}