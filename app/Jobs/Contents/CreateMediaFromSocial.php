<?php
/**
 * Created by PhpStorm.
 * User: ANDROMEDA
 * Date: 2/20/2018
 * Time: 12:45 PM
 */

namespace App\Jobs\Contents;


use App\Core\Contracts\Databases\ContentTableInterfaces;
use App\Models\Contents\Media;
use Carbon\Carbon;

class CreateMediaFromSocial
{
    protected $user;

    protected $model;

    public function __construct($user, ContentTableInterfaces $model)
    {
        $this->user = $user;
        $this->model = $model;
    }

    public function handle()
    {
        $media = [];
        array_push($media, new Media([
            'slug' => $this->getSlugByName($this->user),
            'src' => $this->user->avatar_original
        ]));

        $this->model->media()->saveMany($media);

        return $this->user;
    }

    protected function getSlugByName($user)
    {
        $str = hash('crc32b', $user->name . uniqid()) . '-' . Carbon::now();
        return $str;
    }
}