<?php
/**
 * Created by PhpStorm.
 * User: ANDROMEDA
 * Date: 2/19/2018
 * Time: 7:08 PM
 */

namespace App\Jobs\Contents;

use App\Core\Patch\JobPatcher;
use App\Models\Contents\Post;
use Illuminate\Http\Request;

class CreatePost extends JobPatcher
{

    protected $validateRules = [
        'name' => 'required',
        'images.*' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
        'title' => 'required',
        'body' => 'required',
    ];

    protected $post;

    /**
     * CreatePost constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->post = new Post();
    }

    /**
     * @return Post|mixed
     */
    public function run()
    {
        $this->post->user()->associate(auth()->user());

         $this->post->fill([
             'title' => $this->request->input('title'),
             'body' => $this->request->input('body'),
             'slug' => $this->createSlug($this->request->input('title')),
             'type' => $this->request->input('type')
         ]);

         $this->post->save();

         return $this->post;
    }

    public function callback()
    {
        if ($this->request->hasFile('images'))
            dispatch();
    }

    /**
     * Create slug
     */
    protected function createSlug($title)
    {
        $str = strtolower($title);
        $slug = str_slug($str, '-').'-'.hash('crc32b', uniqid());

        return $slug;
    }

}