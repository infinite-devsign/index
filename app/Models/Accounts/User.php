<?php

namespace App\Models\Accounts;

use App\Core\Contracts\Databases\ContentTableInterfaces;
use App\Models\Contents\Media;
use App\Models\Contents\Post;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements ContentTableInterfaces
{
    use Notifiable;

    const ADMIN = 'admin';
    const MEMBER = 'member';
    const GUEST = 'guest';

    protected $table = 'users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'username', 'email', 'password', 'role', 'ban'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'ban' => 'boolean'
    ];

    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = bcrypt($password);
    }

    public function userMetaData()
    {
        return $this->hasOne(UserMetaData::class, 'user_id', 'id');
    }

    public function posts()
    {
        return $this->hasMany(Post::class, 'user_id', 'id');
    }

    public function media()
    {
        return $this->morphToMany(Media::class, 'mediable', 'mediables');
    }

    public function notifications()
    {
        return $this->belongsToMany(UserNotification::class, 'notif_pivot', 'user_id' , 'notif_id');
    }

    public function isMember()
    {
        return $this->attributes['role'] = self::MEMBER ? true : false;
    }

    public function isAdmin()
    {
        return $this->attributes['role'] = self::ADMIN ? true : false;
    }

    public function isGuest()
    {
        return $this->attributes['role'] = self::GUEST ? true : false;
    }
}
