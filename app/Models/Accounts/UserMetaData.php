<?php

namespace App\Models\Accounts;

use Illuminate\Database\Eloquent\Model;

class UserMetaData extends Model
{
    protected $table = 'user_meta_datas';

    protected $fillable = [
        'address', 'city', 'postal_code', 'birthday', 'gender'
    ];

    public function user()
    {
        return $this->hasOne(User::class, 'user_id', 'id');
    }
}
