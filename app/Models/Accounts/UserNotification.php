<?php

namespace App\Models\Accounts;

use Illuminate\Database\Eloquent\Model;

class UserNotification extends Model
{
    protected $table = 'user_notifications';

    protected $fillable = [
        'user_id', 'title', 'message', 'type'
    ];

    public function users()
    {
        return $this->belongsToMany(User::class, 'notif_pivot', 'notif_id' , 'user_id');
    }
}
