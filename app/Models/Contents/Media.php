<?php

namespace App\Models\Contents;

use App\Models\Accounts\User;
use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    protected $table = 'media';

    protected $fillable = [
        'slug', 'src'
    ];

    public function posts()
    {
        return $this->morphedByMany(Post::class, 'mediable', 'mediables');
    }

    public function users()
    {
        return $this->morphedByMany(User::class, 'mediable', 'mediables');
    }
}
