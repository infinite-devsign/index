<?php

namespace App\Models\Contents;

use App\Models\Accounts\User;
use Illuminate\Database\Eloquent\Model;

class Post extends Model implements ContentTableInterfaces
{
    protected $table = 'posts';
    
    protected $fillable = [
        'user_id', 'title', 'body', 'type'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function media()
    {
        return $this->morphToMany(Media::class, 'mediable', 'mediables');
    }
}
