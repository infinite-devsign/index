@extends('layouts.post')
@section('css')
<style>
.card-img {
    max-height: 300px;
    overflow: hidden;
}
.card-img > img {
    width: 100%;
    display: block;
    margin-top: auto;
    margin-bottom: auto;
}
.card-value {
    margin: 12px;
    font-size: 14px;
    font-weight: 300;
}
</style>
@endsection

@section('content')
<div class="col-md-8">
    <div class="card">
        <div class="card-img">
            <img src="{{ asset('img/example/1.jpg') }}" alt="">
        </div>
        <div class="card-header">
            <h1 class="card-title"><strong>Awesome Gallery Blog Post</strong></h1>
        </div>
        <div class="card-value">
            <p>Holisticly maximize team building ROI via next-generation resources. Enthusiastically promote team driven customer service and error-free solutions. Dynamically myocardinate vertical leadership for synergistic "outside the box" thinking. Efficiently extend global.</p>
        </div>
    </div>    
</div>
<div class="col-md-4">
    <div class="card">
        <div class="card-header">
            <h5 class="card-title"><i class="ti-comment-alt"></i>   <strong>COMMENTS</strong></h5>
        </div>
        <div class="media-list media-list-divided bg-lighter">
            <div class="media">
                <a class="avatar" href="#">
                <img src="{{ asset('img/avatar1.jpg') }}" alt="...">
                </a>
                <div class="media-body">
                <p>
                    <a href="#"><strong>Frank Camley</strong></a>
                    <time class="float-right text-fade" datetime="2017-07-14 20:00">Just now</time>
                </p>
                <p>Uniquely enhance world-class channels with just in time schemas.</p>

                </div>
            </div>

            <div class="media">
                <a class="avatar" href="#">
                <img src="{{ asset('img/avatar2.jpg') }}" alt="...">
                </a>
                <div class="media-body">
                <p>
                    <a href="#"><strong>Tim Hank</strong></a>
                    <time class="float-right text-fade" datetime="2017-07-14 20:00">2 hours ago</time>
                </p>
                <p>Continually drive user friendly solutions through performance based infomediaries.</p>
                </div>
            </div>
            </div>


            <form class="publisher bt-1 border-fade bg-white">
            <img class="avatar avatar-sm" src="{{ asset('img/avatar4.jpg') }}" alt="...">
            <input class="publisher-input" type="text" placeholder="Add Your Comment">
            <a class="publisher-btn"><i class="ti-comment-alt"></i></a>
            </form>

        </div>
    </div>
</div>
@endsection