@extends('layouts.auth')
@section('content')
<div class="col-12">
    <div class="card card-shadowed px-50 py-30 w-400px mx-auto" style="max-width: 100%">
        <h5 class="text-uppercase">Sign in</h5>
        <br>
        <form class="form-type-material">
            <div class="form-group">
                <input type="text" class="form-control" id="username" name="username">
                <label for="username">Username</label>
            </div>
            <div class="form-group">
                <input type="password" class="form-control" id="password" name="password">
                <label for="password">Password</label>
            </div>
            <div class="form-group flexbox flex-column flex-md-row">
                <label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" checked>
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">Remember me</span>
                </label>
                <a class="text-muted hover-primary fs-13 mt-2 mt-md-0" href="#">Forgot password?</a>
            </div>
            <div class="form-group">
                <button class="btn btn-bold btn-block btn-primary" type="submit">Login</button>
            </div>
        </form>

        <div class="divider">Or Sign In With</div>
            <div class="text-center">
                <a class="btn btn-square btn-facebook" href="{{ url('/redirect/facebook') }}"><i class="fa fa-facebook"></i></a>
                <a class="btn btn-square btn-google" href="{{ url('/redirect/google') }}"><i class="fa fa-google"></i></a>
                <a class="btn btn-square btn-twitter" href="{{ url('/redirect/twitter') }}"><i class="fa fa-twitter"></i></a>
            </div>
        </div>
        <p class="text-center text-muted fs-13 mt-20">Don't have an account? <a class="text-primary fw-500" href="#">Sign up</a></p>
    </div>
</div>
@endsection