@extends('layouts.auth')
@section('content')
<div class="col-12">
    <div class="card card-shadowed px-50 py-30 w-400px mx-auto" style="max-width: 100%">
        <h5 class="text-uppercase">Create an account</h5>
        <br>

        <form class="form-type-material" method="post" action="{{ route('register') }}">
            {{ csrf_field() }}
            <div class="form-group">
                <input type="text" name="name" class="form-control" id="name">
                <label for="name">Name</label>
            </div>
            <div class="form-group">
                <input type="text" name="username" class="form-control" id="username">
                <label for="username">Username</label>
            </div>

            <div class="form-group">
                <input type="email" name="email" class="form-control" id="email">
                <label for="email">Email address</label>
            </div>

            <div class="form-group">
                <input type="password" name="password" class="form-control" id="password">
                <label for="password">Password</label>
            </div>

            <div class="form-group">
                <input type="password" class="form-control" name="password_confirmation" id="password_confirmation">
                <label for="password-conf">Password (confirm)</label>
            </div>

            <div class="form-group">
                <label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input">
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">I agree to all <a class="text-primary" href="#">terms</a></span>
                </label>
            </div>

            <br>
            <button class="btn btn-bold btn-block btn-primary" type="submit">Register</button>
        </form>
    </div>
    <p class="text-center text-muted fs-13 mt-20">Already have an account? <a class="text-primary fw-500" href="#">Sign in</a></p>
</div>
@endsection