@extends('layouts.post')

@section('content')
          <div class="col-md-6">
            <div class="card card-inverse bg-img text-center py-90" style="background-image: url({{ asset('img/example/4.png') }})" data-overlay="5">
              <div class="card-body">
                <span class="bb-1 opacity-80 pb-2">16 October, 2016</span>
                <br><br>
                <h3>Awesome Quote Blog Post</h3>
                <br><br>
                <a class="btn btn-outline btn-bold no-radius btn-light" href="#">Read More</a>
              </div>
            </div>
          </div>


          <div class="col-md-6">
            <div class="card">
              <figure class="img-hov-zoomin" style="margin:0;">
                <img src="{{ asset('img/example/1.jpg') }}" alt="...">
              </figure>

              <div class="card-body">
                <ul class="nav nav-dot-separated no-gutters mb-1">
                  <li class="nav-item">
                    <a class="nav-link" href="#">16 Oct</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#"><i class="ti-heart fs-13 mr-1"></i> 15</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#"><i class="ti-comment fs-13 mr-1"></i> 4</a>
                  </li>
                </ul>

                <h4><a class="hover-primary" href="#">Awesome Gallery Blog Post</a></h4>

                <p>Holisticly maximize team building ROI via next-generation resources. Enthusiastically promote team driven customer service and error-free solutions. Dynamically myocardinate vertical leadership for synergistic "outside the box" thinking. Efficiently extend global.</p>

                <p class="text-right">
                  <a class="btn btn-round btn-bold btn-primary" href="#">Read more</a>
                </p>
              </div>
            </div>
          </div>
@endsection