<!DOCTYPE html>
<html lang="en">
  <head>
    @include('layouts.elements.header')
    @yield('css')
  </head>

  <body>
  <script>
      window.fbAsyncInit = function() {
          FB.init({
              appId      : '1766139883693824',
              xfbml      : true,
              version    : 'v2.12'
          });
          FB.AppEvents.logPageView();
      };

      (function(d, s, id){
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) {return;}
          js = d.createElement(s); js.id = id;
          js.src = "https://connect.facebook.net/en_US/sdk.js";
          fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));
  </script>
    <!-- Preloader -->
    <div class="preloader">
      <div class="spinner-dots">
        <span class="dot1"></span>
        <span class="dot2"></span>
        <span class="dot3"></span>
      </div>
    </div>
    <div class="row min-h-fullscreen center-vh p-20 m-0">
    @yield('content')
    </div>


    @include('layouts.elements.footer')




    @include('layouts.elements.scripts')
    @yield('scripts')
  </body>
</html>