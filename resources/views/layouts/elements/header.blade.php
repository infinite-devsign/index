<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="Responsive admin dashboard and web application ui kit. ">
<meta name="keywords" content="login, signin">
<title>{{ $title or '' }} @php if(isset($title)){echo "-";} @endphp DEVSIGN</title>
{{--  <title>DEVSIGN</title>  --}}
<!-- Favicons -->
<link rel="icon" href="{{ asset('img/favicon.png') }}">
<!-- Fonts -->
<link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,300i" rel="stylesheet">

<!-- Styles -->
<link href="{{ asset('css/core.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/app.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/style.min.css') }}" rel="stylesheet">