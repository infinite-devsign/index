<!-- Topbar -->
<header class="topbar topbar-expand-lg topbar-secondary topbar-inverse">
    <div class="container">
    <div class="topbar-left">
        <span class="topbar-btn topbar-menu-toggler"><i>&#9776;</i></span>

        <div class="topbar-brand">
        <a href="index.html"><img src="{{ asset('img/logo.png') }}" alt="..."></a>
        </div>

        <div class="topbar-divider d-none d-md-block"></div>

        <nav class="topbar-navigation">
        <ul class="menu">

            <li class="menu-item active">
            <a class="menu-link" href="index.html">
                <span class="title">Articles</span>
            </a>
            </li>

            <li class="menu-item">
            <a class="menu-link" href="category.html">
                <span class="title">Members</span>
            </a>
            </li>

            <li class="menu-item">
            <a class="menu-link" href="ticket-list.html">
                <span class="title">Profile</span>
            </a>
            </li>

            <li class="menu-item">
            <a class="menu-link" href="forum-list.html">
                <span class="title">Projects</span>
            </a>
            </li>

        </ul>
        </nav>
    </div>


    <div class="topbar-right">

        <ul class="topbar-btns">
        <li>
            <button class="btn btn-w-md btn-round btn-purple" style="margin-top:20px">LOGIN</button>
        </li>
        <li class="dropdown">
            <span class="topbar-btn" data-toggle="dropdown"><img style="border: 1px solid" class="avatar" src="{{ asset('img/avatar4.jpg') }}" alt="..."></span>
            <div class="dropdown-menu dropdown-menu-right">
            <a class="dropdown-item" href="#"><i class="ti-user"></i> Profile</a>
            <a class="dropdown-item" href="#"><i class="ti-panel"></i> Panel</a>
            <a class="dropdown-item" href="#"><i class="ti-settings"></i> Settings</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#"><i class="ti-power-off"></i> Logout</a>
            </div>
        </li>


        <li>
            <span class="topbar-btn has-new" data-toggle="quickview" data-target="#qv-notifications"><i class="ti-bell"></i></span>
        </li>

        </ul>

    </div>
    </div>
</header>
<!-- END Topbar -->