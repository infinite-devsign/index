
<!DOCTYPE html>
<html lang="en">
  <head>
    @include('layouts.elements.header')
    @yield('css')
  </head>

  <body class="topbar-unfix">
    <!-- Preloader -->
    <div class="preloader">
      <div class="spinner-dots">
        <span class="dot1"></span>
        <span class="dot2"></span>
        <span class="dot3"></span>
      </div>
    </div>


@include('layouts.elements.topbar')



    <!-- Main container -->
    <main>


      <header class="header header-inverse">
        <div class="container">
          <div class="header-info">
            <div class="left">
              <br>
              <h2 class="header-title"><strong>DEVSIGN</strong> <small class="subtitle">Future developer and designer.</small></h2>
            </div>
            <div class="right flex-middle d-none d-md-block">
              <form class="lookup lookup-circle lookup-lg lookup-right" target="index.html">
                <input type="text" name="s">
              </form>
            </div>
          </div>
        </div>
      </header><!--/.header -->





      <div class="main-content">
        <div class="container">
          <div class="row">
            @yield('content')
          </div>
        </div>
      </div>





      <!-- Footer -->
      <footer class="site-footer">
        <div class="container">
          <div class="row">
            <div class="col-md-6">
              <p class="text-center text-sm-left">Designed using <a href="http://thetheme.io/theadmin"><strong>TheAdmin</strong></a></p>
            </div>

            <div class="col-md-6">
              <ul class="nav nav-primary nav-dotted nav-dot-separated justify-content-center justify-content-md-end">
                <li class="nav-item">
                  <a class="nav-link" href="#">Help</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">About</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">Contact</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </footer>
      <!-- END Footer -->


    </main>





    <!--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->
    <!-- Quickviews -->


    <!-- Notifications -->
    <div id="qv-notifications" class="quickview">
      <header class="quickview-header quickview-header-lg">
        <h5 class="quickview-title">Notifications</h5>
        <span class="close"><i class="ti-close"></i></span>
      </header>

      <div class="quickview-body">
        <div class="media-list media-list-hover media-list-divided media-sm">
          <a class="media media-new" href="#">
            <span class="avatar bg-success"><i class="ti-user"></i></span>
            <div class="media-body">
              <p>New user registered</p>
              <time datetime="2017-07-14 20:00">Just now</time>
            </div>
          </a>

          <a class="media media-new" href="#">
            <span class="avatar bg-info"><i class="ti-shopping-cart"></i></span>
            <div class="media-body">
              <p>New order received</p>
              <time datetime="2017-07-14 20:00">2 min ago</time>
            </div>
          </a>

          <a class="media media-new" href="#">
            <span class="avatar bg-warning"><i class="ti-face-sad"></i></span>
            <div class="media-body">
              <p>Refund request from <b>Ashlyn Culotta</b></p>
              <time datetime="2017-07-14 20:00">24 min ago</time>
            </div>
          </a>

          <a class="media media-new" href="#">
            <span class="avatar bg-primary"><i class="ti-money"></i></span>
            <div class="media-body">
              <p>New payment has made through PayPal</p>
              <time datetime="2017-07-14 20:00">53 min ago</time>
            </div>
          </a>

          <a class="media" href="#">
            <span class="avatar bg-danger"><i class="ti-package"></i></span>
            <div class="media-body">
              <p>Package lost on the way!</p>
              <time datetime="2017-07-14 20:00">1 hour ago</time>
            </div>
          </a>

          <a class="media" href="#">
            <span class="avatar bg-success"><i class="ti-user"></i></span>
            <div class="media-body">
              <p>New user registered</p>
              <time datetime="2017-07-14 20:00">1 hour ago</time>
            </div>
          </a>

          <a class="media" href="#">
            <span class="avatar bg-purple"><i class="ti-comment"></i></span>
            <div class="media-body">
              <p>New review on <em>iPhone 6s</em></p>
              <time datetime="2017-07-14 20:00">3 hours ago</time>
            </div>
          </a>

          <a class="media" href="#">
            <span class="avatar bg-info"><i class="ti-shopping-cart"></i></span>
            <div class="media-body">
              <p>New order received</p>
              <time datetime="2017-07-14 20:00">5 hours ago</time>
            </div>
          </a>

          <a class="media" href="#">
            <span class="avatar bg-danger"><i class="fa fa-area-chart"></i></span>
            <div class="media-body">
              <p>CPU usage went above 80%</p>
              <time datetime="2017-07-14 20:00">Yesterday</time>
            </div>
          </a>

          <a class="media" href="#">
            <span class="avatar bg-yellow"><i class="fa fa-star"></i></span>
            <div class="media-body">
              <p>New rating on iPhone 6s, 5 star</p>
              <time datetime="2017-07-14 20:00">Yesterday</time>
            </div>
          </a>

          <a class="media" href="#">
            <span class="avatar bg-success"><i class="ti-user"></i></span>
            <div class="media-body">
              <p>New user registered</p>
              <time datetime="2017-07-14 20:00">Yesterday</time>
            </div>
          </a>

          <a class="media" href="#">
            <span class="avatar bg-primary"><i class="ti-money"></i></span>
            <div class="media-body">
              <p>New payment has made through PayPal</p>
              <time datetime="2017-07-14 20:00">2 days ago</time>
            </div>
          </a>

          <a class="media" href="#">
            <span class="avatar bg-info"><i class="ti-shopping-cart"></i></span>
            <div class="media-body">
              <p>New order received</p>
              <time datetime="2017-07-14 20:00">2 days ago</time>
            </div>
          </a>

          <a class="media" href="#">
            <span class="avatar bg-purple"><i class="ti-comment"></i></span>
            <div class="media-body">
              <p>New review on Samsung Galaxy S7</p>
              <time datetime="2017-07-14 20:00">Aug 17</time>
            </div>
          </a>
        </div>
      </div>

      @include('layouts.elements.footer')
    </div>


    @include('layouts.elements.scripts')
    @yield('scripts')
  </body>
</html>
