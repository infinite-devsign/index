
<!DOCTYPE html>
<html lang="en">
  <head>
    @include('layouts.elements.header')
  </head>

  <body>


    <div class="row no-margin h-fullscreen" style="padding-top: 10%">

      <div class="col-12">
        <div class="card card-transparent center-h text-center">
          <h1 class="text-secondary lh-1" style="font-size: 200px"><i class="fa fa-gear"></i></h1>
          <hr class="w-30px">
          <h3 class="text-uppercase">Maintenance Mode</h3>

          <p class="lead">Lagi maintenance ya gan!<br>Jan di ganggu!</p>
        </div>
      </div>


      <footer class="col-12 align-self-end text-center fs-13">
        <p>Copyright © 2018 <a href="http://devsign.io">DEVSIGN</a>. Made With <i class="ion-ios-heart" style="color:red; margin: 0 3px;"></i> in Surabaya</p>
      </footer>
    </div>




    <!-- Scripts -->
    @include('layouts.elements.scripts')

  </body>
</html>
