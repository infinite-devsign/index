@extends('layouts.panel')
@section('content')
<div class="card">
    <div class="card-header">
    <h5 class="card-title"><strong>Draft</strong></h5>
    <a class="btn btn-w-md btn-success btn-label" href="#"><label><i class="ti-plus"></i></label> Create</a>
    </div>

    <table class="table table-hover">
    <thead>
        <tr>
        <th>Name</th>
        <th>Categori</th>
        <th>Date</th>
        <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <tr>
        <td>How To Install Meteor JS on Ubuntu</td>
        <td>Framework</td>
        <td>19/03/2018</td>
        <td>
            <nav class="nav no-gutters gap-2 fs-16">
            <a class="nav-link hover-warning" href="#" data-provide="tooltip" title="Post"><i class="ti-export"></i></a>
            <a class="nav-link hover-primary" href="#" data-provide="tooltip" title="Edit"><i class="ti-pencil"></i></a>
            <a class="nav-link hover-danger" href="#" data-provide="tooltip" title="Delete"><i class="ti-trash"></i></a>
            </nav>
        </td>
        </tr>
    </tbody>
    </table>

</div>
@endsection