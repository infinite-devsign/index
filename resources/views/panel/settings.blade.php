@extends('layouts.panel')
@section('content')
<div class="row">


    <div class="col-lg-3">
    <div class="card">
        <ul class="nav nav-lg nav-pills flex-column">
        <li class="nav-item" data-toggle="tab" data-target="#tab2">
            <a class="nav-link" href="#">User Details</a>
        </li>
        </ul>
    </div>
    </div>



    <div class="col-lg-9 tab-content">
    <form class="card form-type-material tab-pane fade active show" id="tab2">
        <h4 class="card-title fw-400">User Details</h4>

        <div class="card-body">
        <div class="flexbox gap-items-4">
            <img class="avatar avatar-xl" src="{{ asset('img/avatar.jpg') }}" alt="...">

            <div class="flex-grow">
            <h5>Hossein Shams</h5>
            <div class="d-felx flex-column flex-sm-row gap-y gap-items-2 mt-16">
                <div class="file-group file-group-inline">
                <button class="btn btn-sm btn-w-lg btn-outline btn-round btn-secondary file-browser" type="button">Change Picture</button>
                <input type="file">
                </div>

                <a class="btn btn-sm btn-w-lg btn-outline btn-round btn-danger align-top" href="#">Delete Picture</a>
            </div>

            </div>
        </div>

        <hr>

        <div class="row">
            <div class="col-md-6">
            <div class="form-group">
                <input class="form-control" type="text">
                <label>First name</label>
            </div>
            </div>

            <div class="col-md-6">
            <div class="form-group">
                <input class="form-control" type="text">
                <label>Last name</label>
            </div>
            </div>
        </div>


        <div class="form-group">
            <input class="form-control" type="text">
            <label>Email</label>
        </div>


        <div class="row">
            <div class="col-md-6">
            <div class="form-group">
                <select class="form-control" title="&#xA0;" data-provide="selectpicker">
                <option>United States</option>
                <option>Canada</option>
                <option>Mexico</option>
                <option>United Kingdom</option>
                </select>
                <label>Country</label>
            </div>
            </div>

            <div class="col-md-6">
            <div class="form-group">
                <input class="form-control" type="text">
                <label>Phone</label>
            </div>
            </div>
        </div>
        </div>

        <footer class="card-footer text-right">
        <button class="btn btn-flat btn-primary" type="submit">Save Changes</button>
        </footer>
    </form>
    </div>


</div>
@endsection
