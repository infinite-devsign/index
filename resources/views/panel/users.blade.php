@extends('layouts.panel')
@section('content')
<div class="card">
        <div class="media-list media-list-divided media-list-hover">
          <div class="media">
            <a class="avatar avatar-lg status-success" href="#">
              <img src="../assets/img/avatar/1.jpg" alt="...">
            </a>

            <div class="media-body">
              <p>
                <a class="hover-primary" href="#"><strong>Maryam Amiri</strong></a>
                <small class="sidetitle">Online</small>
              </p>
              <p>Designer</p>

              <nav class="nav no-gutters mt-2">
                <a class="nav-link" href="#"><i class="fa fa-facebook"></i></a>
                <a class="nav-link" href="#"><i class="fa fa-twitter"></i></a>
                <a class="nav-link" href="#"><i class="fa fa-github"></i></a>
                <a class="nav-link" href="#"><i class="fa fa-linkedin"></i></a>
              </nav>
            </div>

            <div class="media-right gap-items">
              <a class="media-action lead" href="#" data-provide="tooltip" title="Orders"><i class="ti-shopping-cart"></i></a>
              <a class="media-action lead" href="#" data-provide="tooltip" title="Receipts"><i class="ti-receipt"></i></a>
              <div class="btn-group">
                <a class="media-action lead" "=" href="#" data-toggle="dropdown"><i class="ti-more-alt rotate-90"></i></a>
                <div class="dropdown-menu dropdown-menu-right">
                  <a class="dropdown-item" href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                  <a class="dropdown-item" href="#"><i class="fa fa-fw fa-comments"></i> Messages</a>
                  <a class="dropdown-item" href="#"><i class="fa fa-fw fa-phone"></i> Call</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="#"><i class="fa fa-fw fa-remove"></i> Remove</a>
                </div>
              </div>

            </div>
          </div>
        </div>
        <nav class="mt-3">
          <ul class="pagination justify-content-center">
            <li class="page-item disabled">
              <a class="page-link" href="#">
                <span class="ti-arrow-left"></span>
              </a>
            </li>
            <li class="page-item active"><a class="page-link" href="#">1</a></li>
            <li class="page-item"><a class="page-link" href="#">2</a></li>
            <li class="page-item"><a class="page-link" href="#">3</a></li>
            <li class="page-item"><a class="page-link" href="#">4</a></li>
            <li class="page-item"><a class="page-link" href="#">5</a></li>
            <li class="page-item">
              <a class="page-link" href="#">
                <span class="ti-arrow-right"></span>
              </a>
            </li>
          </ul>
        </nav>
</div>
@endsection