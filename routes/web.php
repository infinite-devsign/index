<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.register');
});
Route::post('/', 'Auth\RegisterController@register')->name('register');
Route::get('/login', function () {
    return view('auth.login');
});

Route::get('/home', function () {
    return dd(auth()->user());
})->name('home');

Route::group([
    'as' => 'admin.',
    'prefix' => 'admin',
    'namespace' => 'Auth'
], function () {
    // Authentication Routes...
    $this->get('login.html', 'LoginController@showLoginForm')->name('login');
    $this->post('login.html', 'LoginController@login');
    $this->post('logout.html', 'LoginController@logout')->name('logout');
});